using Intro2CSharp.GameOfLife.ObjectOriented;

namespace Intro2CSharpTest.GameOfLife.ObjectOriented
{
    [TestClass]
    public class CoordinateTest
    {
        private Coordinate c00, c03, c63, c65, c24;
        private Coordinate c01, c02, c11, c04, c13, c53, c64, c25;

        [TestInitialize]
        public void SetUp()
        {
            c00 = new(0, 0);
            c03 = new(0, 3);
            c63 = new(6, 3);
            c65 = new(6, 5);
            c24 = new(2, 4);
            c01 = new(0, 1);
            c02 = new(0, 2);
            c11 = new(1, 1);
            c04 = new(0, 4);
            c13 = new(1, 3);
            c53 = new(5, 3);
            c64 = new(6, 4);
            c25 = new(2, 5);
        }


        [TestMethod]
        public void TestCreate()
        {
            // given
            // when
            // then
            Assert.AreEqual((2, 4), c24.Tuple);
            Assert.ThrowsException<GameOfLifeException>(() => { Coordinate coordinate = new(-1, 0); });
            Assert.ThrowsException<GameOfLifeException>(() => { Coordinate coordinate = new(0, -1); });
        }



        [TestMethod]
        public void TestGreaterAndSmaller()
        {
            // given
            // when
            // then
            Assert.IsTrue(c04 > c03);
            Assert.IsTrue(c63 > c03);
            Assert.IsFalse(c24 > c24);
            Assert.IsFalse(c13 > c24);

            Assert.IsFalse(c04 < c03);
            Assert.IsFalse(c63 < c03);
            Assert.IsFalse(c24 < c24);
            Assert.IsTrue(c13 < c24);
        }



        [TestMethod]
        public void TestNeighboursLeftBorder()
        {
            // given
            // when
            ISet<Coordinate> neighbours03 = c03.NeighbourCoordinates(c65);

            // then
            Assert.AreEqual(neighbours03.Count, 5);
            Assert.IsTrue(neighbours03.Contains(c13));
            Assert.IsTrue(neighbours03.Contains(c04));
            Assert.IsFalse(neighbours03.Contains(c01));
            Assert.IsFalse(neighbours03.Contains(c03));
        }

        [TestMethod]
        public void TestNeighboursRightBorder()
        {
            // given
            // when
            ISet<Coordinate> neighbours63 = c63.NeighbourCoordinates(c65);

            // then
            Assert.AreEqual(neighbours63.Count, 5);
            Assert.IsTrue(neighbours63.Contains(c64));
            Assert.IsTrue(neighbours63.Contains(c53));
            Assert.IsFalse(neighbours63.Contains(c65));
        }

        [TestMethod]
        public void TestNeighboursOrigin()
        {
            // given
            // when
            ISet<Coordinate> neighbours00 = c00.NeighbourCoordinates(c65);

            // then
            Assert.AreEqual(neighbours00.Count, 3);
            Assert.IsTrue(neighbours00.Contains(c01));
            Assert.IsFalse(neighbours00.Contains(c02));
        }

        [TestMethod]
        public void TestNeighboursTopRightCorner()
        {
            // given
            // when
            ISet<Coordinate> neighbours65 = c65.NeighbourCoordinates(c65);

            // then
            Assert.AreEqual(neighbours65.Count, 3);
            Assert.IsTrue(neighbours65.Contains(c64));
            Assert.IsFalse(neighbours65.Contains(c53));
        }


        [TestMethod]
        public void TestNeighboursCenter()
        {
            // given
            // when
            ISet<Coordinate> neighbours24 = c24.NeighbourCoordinates(c65);

            // then
            Assert.AreEqual(neighbours24.Count, 8);
            Assert.IsTrue(neighbours24.Contains(c13));
            Assert.IsFalse(neighbours24.Contains(c53));
            Assert.IsFalse(neighbours24.Contains(c04));
        }

        [TestMethod]
        public void TestInvalidNeighbours()
        {
            // given
            // when
            // then
            Assert.ThrowsException<GameOfLifeException>(() =>
            { ISet<Coordinate> neighbours24 = c24.NeighbourCoordinates(c13); }
            );
            Assert.ThrowsException<GameOfLifeException>(() =>
            { ISet<Coordinate> neighbours65 = c65.NeighbourCoordinates(c63); }
            );
        }

        [TestMethod]
        public void TestToString()
        {
            Assert.AreEqual("(2, 4)", c24.ToString());   
        }

    }
}