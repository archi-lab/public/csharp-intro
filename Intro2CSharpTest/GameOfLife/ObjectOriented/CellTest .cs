using Intro2CSharp.GameOfLife.ObjectOriented;

namespace Intro2CSharpTest.GameOfLife.ObjectOriented
{
    [TestClass]
    public class CellTest
    {
        private Cell cell14, cell24center, cell34, cell15, cell25, cell35, cell13, cell23, cell33;
        private CellCanvas cellCanvas;
        private Coordinate c97, c80_20;

        [TestInitialize]
        public void SetUp()
        {
            c97 = new Coordinate(9, 7);
            cellCanvas = CellCanvas.WithRandomCells((9,7), 0.5);
            cell24center = cellCanvas.GetCellAt(new Coordinate(2, 4));

            cell14 = cellCanvas.GetCellAt(new Coordinate(1, 4));
            cell34 = cellCanvas.GetCellAt(new Coordinate(3, 4));
            cell15 = cellCanvas.GetCellAt(new Coordinate(1, 5));
            cell25 = cellCanvas.GetCellAt(new Coordinate(2, 5));
            cell35 = cellCanvas.GetCellAt(new Coordinate(3, 5));
            cell13 = cellCanvas.GetCellAt(new Coordinate(1, 3));
            cell23 = cellCanvas.GetCellAt(new Coordinate(2, 3));
            cell33 = cellCanvas.GetCellAt(new Coordinate(3, 3));
        }


        [TestMethod]
        public void TestStarvingCell()
        {
            // given
            cell14.IsAliveNow = false;
            cell34.IsAliveNow = false;
            cell15.IsAliveNow = true;
            cell25.IsAliveNow = false;
            cell35.IsAliveNow = false;
            cell13.IsAliveNow = false;
            cell23.IsAliveNow = false;
            cell33.IsAliveNow = false;

            // when
            cell24center.CalculateNextStep();
            cell24center.SwitchToNextStep();

            // then
            Assert.IsFalse(cell24center.IsAliveNow);
        }

        [TestMethod]
        public void TestHappyCell()
        {
            // given
            cell14.IsAliveNow = false;
            cell34.IsAliveNow = false;
            cell15.IsAliveNow = true;
            cell25.IsAliveNow = false;
            cell35.IsAliveNow = false;
            cell13.IsAliveNow = true;
            cell23.IsAliveNow = false;
            cell33.IsAliveNow = false;

            // when
            cell24center.CalculateNextStep();
            cell24center.SwitchToNextStep();

            // then
            Assert.IsTrue(cell24center.IsAliveNow);
        }


        [TestMethod]
        public void TestEvenHappierCell()
        {
            // given
            cell14.IsAliveNow = false;
            cell34.IsAliveNow = false;
            cell15.IsAliveNow = true;
            cell25.IsAliveNow = false;
            cell35.IsAliveNow = false;
            cell13.IsAliveNow = true;
            cell23.IsAliveNow = false;
            cell33.IsAliveNow = true;

            // when
            cell24center.CalculateNextStep();
            cell24center.SwitchToNextStep();

            // then
            Assert.IsTrue(cell24center.IsAliveNow);
        }


        [TestMethod]
        public void TestSuffocatedCell()
        {
            // given
            cell14.IsAliveNow = true;
            cell34.IsAliveNow = false;
            cell15.IsAliveNow = true;
            cell25.IsAliveNow = false;
            cell35.IsAliveNow = false;
            cell13.IsAliveNow = true;
            cell23.IsAliveNow = false;
            cell33.IsAliveNow = true;

            // when
            cell24center.CalculateNextStep();
            cell24center.SwitchToNextStep();

            // then
            Assert.IsFalse(cell24center.IsAliveNow);
        }


        [TestMethod]
        public void TestNewBornCell()
        {
            // given
            cell14.IsAliveNow = true;
            cell34.IsAliveNow = false;
            cell15.IsAliveNow = true;
            cell25.IsAliveNow = false;
            cell35.IsAliveNow = false;
            cell13.IsAliveNow = true;
            cell23.IsAliveNow = false;
            cell33.IsAliveNow = false;

            // when
            cell24center.IsAliveNow = false;
            cell24center.CalculateNextStep();
            cell24center.SwitchToNextStep();

            // then
            Assert.IsTrue(cell24center.IsAliveNow);
        }

        [TestMethod]
        public void TestTooSuffocatedForBirth()
        {
            // given
            cell14.IsAliveNow = true;
            cell34.IsAliveNow = false;
            cell15.IsAliveNow = true;
            cell25.IsAliveNow = false;
            cell35.IsAliveNow = false;
            cell13.IsAliveNow = true;
            cell23.IsAliveNow = true;
            cell33.IsAliveNow = false;

            // when
            cell24center.IsAliveNow = false;
            cell24center.CalculateNextStep();
            cell24center.SwitchToNextStep();

            // then
            Assert.IsFalse(cell24center.IsAliveNow);
        }



        [TestMethod]
        public void TestTooStarvedForBirth()
        {
            // given
            cell14.IsAliveNow = true;
            cell34.IsAliveNow = false;
            cell15.IsAliveNow = true;
            cell25.IsAliveNow = false;
            cell35.IsAliveNow = false;
            cell13.IsAliveNow = false;
            cell23.IsAliveNow = false;
            cell33.IsAliveNow = false;

            // when
            cell24center.IsAliveNow = false;
            cell24center.CalculateNextStep();
            cell24center.SwitchToNextStep();

            // then
            Assert.IsFalse(cell24center.IsAliveNow);
        }


        [TestMethod]
        public void TestPrintedStateOk()
        {
            // given
            cell14.IsAliveNow = true;
            cell34.IsAliveNow = false;

            // when
            // then
            Assert.AreEqual(" ", cell34.ToString());
            Assert.AreNotEqual(" ", cell14.ToString());
        }
    }
}