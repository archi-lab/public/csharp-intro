using Intro2CSharp.GameOfLife.ObjectOriented;

namespace Intro2CSharpTest.GameOfLife.ObjectOriented
{
    [TestClass]
    public class CellCanvasTest
    {
        private CellCanvas cellCanvas;
        private Coordinate c97, c80_20;

        [TestInitialize]
        public void SetUp()
        {
            c97 = new Coordinate(9, 7);
            c80_20 = new Coordinate(80, 20);
        }

        [TestMethod]
        public void TestConstructor()
        {
            // given
            cellCanvas = new(c97);

            // when
            // then
            Assert.AreEqual(cellCanvas.Cells.GetLength(0), 9+1);
            Assert.AreEqual(cellCanvas.Cells.GetLength(1), 7+1);
            Assert.AreEqual(cellCanvas.TopRightCorner, c97);
        }

        [TestMethod]
        public void TestFillRandomWithLivingCells()
        {
            // given
            cellCanvas = new(c97); 

            // when
            cellCanvas.FillRandom(1);

            // then
            Assert.AreEqual((9+1) * (7+1), cellCanvas.CountLivingCells());
        }

        [TestMethod]
        public void TestFillRandomWithDeadCells()
        {
            // given
            cellCanvas = new(c97);

            // when
            cellCanvas.FillRandom(0);

            // then
            Assert.AreEqual(0, cellCanvas.CountLivingCells());
        }


        [TestMethod]
        public void TestRealisticInitialization()
        {
            // given
            // when
            cellCanvas = CellCanvas.WithRandomCells((80,20), 0.4);
            // then
            Assert.IsTrue(cellCanvas.CountLivingCells() > 0);
            Assert.IsTrue(cellCanvas.CountLivingCells() < 1600);
        }

    }
}