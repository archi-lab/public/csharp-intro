﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Intro2CSharp
{
    public class ControlStructureExamples
    {
        public void ReadStringFromConsole()
        {
            Console.Write("Was ist deine Eingabe? ");
            string? inputString = Console.ReadLine();
            Console.WriteLine($"Deine Eingabe war '{inputString}'.");
        }

        public void ReadIntFromConsole()
        {
            Console.Write("Was ist deine Eingabe? ");
            string? inputString = Console.ReadLine();
            int inputInt = Convert.ToInt16(inputString);
            Console.WriteLine($"Deine Eingabe war '{inputInt}'.");
        }

        public void ReadDoubleFromConsole()
        {
            Console.Write("Was ist deine Eingabe? ");
            string? inputString = Console.ReadLine();
            double inputDouble = Convert.ToDouble(inputString);
            Console.WriteLine($"Deine Eingabe war '{inputDouble}'.");
            Console.WriteLine($"Zum Quadrat ist das '{inputDouble * inputDouble}'.");
        }

        public void CompareIntAndDouble()
        {
            Console.Write("Was ist deine int-Eingabe? ");
            string? inputString = Console.ReadLine();
            int inputInt = Convert.ToInt16(inputString);
            Console.WriteLine($"Deine Eingabe war '{inputInt}'.");

            Console.Write("Was ist deine double-Eingabe? ");
            inputString = Console.ReadLine();
            double inputDouble = Convert.ToDouble(inputString);
            Console.WriteLine($"Deine Eingabe war '{inputDouble}'.");

            if (inputDouble > inputInt)
            {
                Console.WriteLine($"{inputDouble} ist größer!");
            }
            else
            {
                Console.WriteLine($"{inputInt} ist größer");
            }
            Console.WriteLine($"Das Produkt ist '{inputInt * inputDouble}'.");
        }


        public void CompareIntAndDoubleUsingBool()
        {
            Console.Write("Was ist deine int-Eingabe? ");
            string? inputString = Console.ReadLine();
            int inputInt = Convert.ToInt16(inputString);
            Console.WriteLine($"Deine Eingabe war '{inputInt}'.");

            Console.Write("Was ist deine double-Eingabe? ");
            inputString = Console.ReadLine();
            double inputDouble = Convert.ToDouble(inputString);
            Console.WriteLine($"Deine Eingabe war '{inputDouble}'.");

            bool isGreater = inputDouble > inputInt;
            Console.WriteLine($"Auswertung von inputDouble > inputInt ergibt '{isGreater}'");
            if (isGreater) Console.WriteLine("Hab ichs doch gesagt!");
        }


        public void ForLoop()
        {
            Console.Write("Was ist deine Eingabe? ");
            string? inputString = Console.ReadLine();
            int inputInt = Convert.ToInt16(inputString);
            Console.WriteLine($"Jetzt schreibe ich {inputInt}mal 'Hallo'.");
            for (int i = 0; i < inputInt; i++)
            {
                Console.WriteLine($"Hallo! ({i})");
            }

        }


        public void WhileLoop()
        {
            Console.WriteLine("Ich mache so lange weiter, bis du 'stop' sagst ...");
            string? inputString = "weiter";
            while (inputString != "stop")
            {
                Console.Write("Sag was! ");
                inputString = Console.ReadLine();
            }
        }

        public void WhileLoopUsingEquals()
        {
            Console.WriteLine("Ich mache so lange weiter, bis du 'stop' sagst ...");
            string? inputString = "weiter";
            while (!"stop".Equals(inputString))
            {
                Console.Write("Sag was! ");
                inputString = Console.ReadLine();
            }
        }


        public void DoLoop()
        {
            Console.WriteLine("Ich mache so lange weiter, bis du 'stop' sagst ...");
            string? inputString;
            do
            {
                Console.Write("Sag was! ");
                inputString = Console.ReadLine();
            }
            while (inputString != "stop");
        }


    }
}
