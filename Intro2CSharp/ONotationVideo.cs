﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace Intro2CSharp
{
    public class ONotationExamples
    {


        public void PerformanceComparisonO1()
        {
            const long SIZE = 500000;
            Dictionary<long, string> hashMap = new();
            string[] stringArray = new string[SIZE];
            string currentString;

            Console.WriteLine($"Initialize with size {SIZE}.");
            for (int i = 0; i < SIZE; i++)
            {
                currentString = "Number" + i;
                hashMap.Add(i, currentString);
                stringArray[i] = currentString;
            }

            const long NUM_LOOPS = 400000;
            Stopwatch stopwatch = new Stopwatch();

            Console.WriteLine($"Access Array {NUM_LOOPS} times ...");
            stopwatch.Start();
            for (int i = 0; i < NUM_LOOPS; i++)
            {
                long currentIndex = SIZE / (i + 2);
                currentString = stringArray[currentIndex];
                stringArray[currentIndex] = currentString;
            }
            stopwatch.Stop();
            TimeSpan stopwatchElapsed = stopwatch.Elapsed;
            Console.WriteLine($"This took {Convert.ToInt32(stopwatchElapsed.TotalMilliseconds)} millisecs.");


            Console.WriteLine($"Access HashMap {NUM_LOOPS} times ...");
            stopwatch.Start();
            for (int i = 0; i < NUM_LOOPS; i++)
            {
                long currentIndex = SIZE / (i + 2);
                currentString = hashMap[currentIndex];
                hashMap[currentIndex] = currentString;
            }
            stopwatch.Stop();
            stopwatchElapsed = stopwatch.Elapsed;
            Console.WriteLine($"This took {Convert.ToInt32(stopwatchElapsed.TotalMilliseconds)} millisecs.");
        }



        public void PerformanceComparisonListVsHashSet()
        {
            const long SIZE = 100000;
            HashSet<string> set = new();
            List<string> list = new();
            var randomGenerator = new Random();
            string currentString;

            Console.WriteLine($"Initialize with size {SIZE}.");
            for (int i = 0; i < SIZE; i++)
            {
                currentString = "Number" + randomGenerator.NextInt64(SIZE);
                set.Add(currentString);
                list.Add(currentString);
            }

            const long NUM_LOOPS = 1000;
            Stopwatch stopwatch = new Stopwatch();

            Console.WriteLine($"Access Set {NUM_LOOPS} times ...");
            long numOfMatches = 0;
            stopwatch.Start();
            for (int i = 0; i < NUM_LOOPS; i++)
            {
                long currentIndex = SIZE / (i + 2);
                currentString = "Number" + currentIndex;
                if (set.Contains(currentString)) numOfMatches++;
            }
            stopwatch.Stop();
            TimeSpan stopwatchElapsed = stopwatch.Elapsed;
            Console.WriteLine($"{numOfMatches} matches, this took {Convert.ToInt32(stopwatchElapsed.TotalMilliseconds)} millisecs.");


            Console.WriteLine($"Access List {NUM_LOOPS} times ...");
            numOfMatches = 0;
            stopwatch.Start();
            for (int i = 0; i < NUM_LOOPS; i++)
            {
                long currentIndex = SIZE / (i + 2);
                currentString = "Number" + currentIndex;
                if (list.Contains(currentString)) numOfMatches++;
            }
            stopwatch.Stop();
            stopwatchElapsed = stopwatch.Elapsed;
            Console.WriteLine($"{numOfMatches} matches, this took {Convert.ToInt32(stopwatchElapsed.TotalMilliseconds)} millisecs.");
        }


        public void PerformanceComparisonSorting()
        {
            const long SIZE = 100000;
            List<string> list = new();
            string currentString;
            var randomGenerator = new Random();

            Console.WriteLine($"Initialize with size {SIZE}.");
            for (int i = 0; i < SIZE; i++)
            {
                currentString = "Number" + randomGenerator.NextInt64(SIZE);
                list.Add(currentString);
            }

            Stopwatch stopwatch = new Stopwatch();

            Console.WriteLine($"Sorting the list...");
            stopwatch.Start();
            list.Sort();
            stopwatch.Stop();
            TimeSpan stopwatchElapsed = stopwatch.Elapsed;
            Console.WriteLine($"This took {Convert.ToInt32(stopwatchElapsed.TotalMilliseconds)} millisecs.");
        }


    }
}
