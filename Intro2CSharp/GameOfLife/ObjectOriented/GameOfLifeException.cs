﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Intro2CSharp.GameOfLife.ObjectOriented
{
    public class GameOfLifeException : ArgumentNullException 
    {
        public GameOfLifeException (string message) : base(message) { }
    }
}
