﻿using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("Intro2CSharpTest")]

namespace Intro2CSharp.GameOfLife.ObjectOriented
{
    public class Cell
    {
        public Coordinate Coordinate { get; init; }

        public bool IsAliveNow { get; internal set; }
        private bool _isAliveInNextStep;

        public CellCanvas CellCanvas { get; init; }

        public Cell(CellCanvas cellCanvas, Coordinate coordinate, bool isAliveNow)
        {
            CellCanvas = cellCanvas;
            Coordinate = coordinate;
            IsAliveNow = isAliveNow;
        }

        public void CalculateNextStep()
        {
            ISet<Coordinate> neighbourCoordinates = Coordinate.NeighbourCoordinates(CellCanvas.TopRightCorner);
            int countAliveNeighbours = 0;
            foreach (var coordinate in neighbourCoordinates) {
                Cell neighbour = CellCanvas.GetCellAt(coordinate);
                if (neighbour.IsAliveNow) countAliveNeighbours++;
            }
            if (IsAliveNow)
            {
                _isAliveInNextStep = (countAliveNeighbours >= 2 && countAliveNeighbours <= 3);
            }
            else
            {
                _isAliveInNextStep = (countAliveNeighbours == 3);
            }
            
        }

        public void SwitchToNextStep()
        {
            IsAliveNow = _isAliveInNextStep;
        }

        public override string ToString()
        {
            return IsAliveNow ? "\u2588" : " ";
        }

    }
}
