﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Intro2CSharp.GameOfLife.ObjectOriented
{
    public readonly struct Coordinate 
    {
        public (int X, int Y) Tuple { get; init; }

        public Coordinate((int x,int y) tuple)
        {
            if (tuple.x < 0 || tuple.y < 0) throw new GameOfLifeException("Coordinate dimensions must not be <0");
            Tuple = tuple;
        }

        public Coordinate(int x, int y)
        {
            if (x < 0 || y < 0) throw new GameOfLifeException("Coordinate dimensions must not be <0");
            Tuple = (x, y);
        }


        /// <summary>
        /// Returns all neighbour coordinates assuming a canvas with boundaries - (0,0) at the bottom
        /// left corner, and a top right corner.
        /// </summary>
        /// <param name="topRightCorner">Canvas boundary to the right (no larger x or y values possible)</param>
        /// <returns>The collection of neighbour coordinates, in no particular order</returns>
        public ISet<Coordinate> NeighbourCoordinates(Coordinate topRightCorner)
        {
            if (this > topRightCorner) throw new GameOfLifeException("Can't get neighbours outside canvas!");

            HashSet<Coordinate> neighbours = new();
            if (Tuple.X > 0) 
                neighbours.Add(new Coordinate(Tuple.X-1, Tuple.Y));
            if (Tuple.X > 0 && Tuple.Y > 0)  
                neighbours.Add(new Coordinate(Tuple.X-1, Tuple.Y-1));
            if (Tuple.X > 0 && Tuple.Y < topRightCorner.Tuple.Y) 
                neighbours.Add(new Coordinate(Tuple.X-1, Tuple.Y+1));
            if (Tuple.Y > 0) 
                neighbours.Add(new Coordinate(Tuple.X, Tuple.Y-1));
            if (Tuple.Y < topRightCorner.Tuple.Y) 
                neighbours.Add(new Coordinate(Tuple.X, Tuple.Y+1));
            if (Tuple.X < topRightCorner.Tuple.X)
                neighbours.Add(new Coordinate(Tuple.X+1, Tuple.Y));
            if (Tuple.X < topRightCorner.Tuple.X && Tuple.Y > 0) 
                neighbours.Add(new Coordinate(Tuple.X+1, Tuple.Y-1));
            if (Tuple.X < topRightCorner.Tuple.X && Tuple.Y < topRightCorner.Tuple.Y) 
                neighbours.Add(new Coordinate(Tuple.X+1, Tuple.Y+1));
            return neighbours;
        }


        public static bool operator >(Coordinate a, Coordinate b)
        {
            return (a.Tuple.X > b.Tuple.X || a.Tuple.Y > b.Tuple.Y);
        }

        public static bool operator <(Coordinate a, Coordinate b)
        {
            return (a.Tuple.X < b.Tuple.X || a.Tuple.Y < b.Tuple.Y);
        }

        public override string ToString() { 
            return Tuple.ToString();
        }
    }
}
