﻿using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("Intro2CSharpTest")]

namespace Intro2CSharp.GameOfLife.ObjectOriented
{
    public class CellCanvas
    {
        internal Cell[,] Cells { get; init; }

        public Coordinate TopRightCorner { get; init; }

        const int SLEEP = 20;

        public static CellCanvas Init()
        {
            CellCanvas cellCanvas = CellCanvas.WithRandomCells((115, 25));
            //(int, int)[] pattern = { (40, 10), (41, 10), (42, 10), (42, 11), (41, 12) };
            //CellCanvas cellCanvas = CellCanvas.WithPattern((115, 25), pattern);
            return cellCanvas;
        }


        public static CellCanvas WithRandomCells((int x, int y) topRightCorner, double livingProbability = 0.1)
        {
            CellCanvas cellCanvas = new(new Coordinate(topRightCorner.x, topRightCorner.y));
            cellCanvas.FillRandom(livingProbability);
            return cellCanvas;
        }

        public static CellCanvas WithPattern((int x, int y) topRightCorner, (int,int)[] patternAliveCells)
        {
            CellCanvas cellCanvas = new(new Coordinate(topRightCorner.x, topRightCorner.y));
            cellCanvas.FillWithPattern(patternAliveCells);
            return cellCanvas;
        }

        internal CellCanvas(Coordinate topRightCorner)
        {
            Cells = new Cell[topRightCorner.Tuple.X+1, topRightCorner.Tuple.Y+1];
            TopRightCorner = topRightCorner;
        }

        internal void FillRandom(double livingProbability)
        {
            Random random = new Random();
            for (int x = 0; x < Cells.GetLength(0); x++)
            {
                for (int y = 0; y < Cells.GetLength(1); y++)
                {
                    bool isAlive = (random.NextDouble() < livingProbability);
                    Cells[x, y] = new Cell(this, new Coordinate(x, y), isAlive);
                }
            }
        }

        internal void FillWithPattern((int, int)[] patternAliveCells)
        {
            for (int x = 0; x < Cells.GetLength(0); x++)
            {
                for (int y = 0; y < Cells.GetLength(1); y++)
                {
                    Coordinate currentCoordinate = new Coordinate(x, y);
                    bool isAlive = patternAliveCells.Contains(currentCoordinate.Tuple);
                    Cells[x, y] = new Cell(this, currentCoordinate, isAlive);
                }
            }
        }


        public void Run()
        {
            while (true)
            {
                PrintCells();
                CalculateNextStepsInCells();
                SwitchCellsToNextStep();
            }
        }

        internal void PrintCells()
        {
            try
            {
                Console.SetCursorPosition(0, Console.WindowTop);
                for (int y = 0; y < Cells.GetLength(1); y++)
                {
                    for (int x = 0; x < Cells.GetLength(0); x++)
                    {
                        Console.SetCursorPosition(x, Cells.GetLength(1) - y);
                        Console.Write(Cells[x, y].ToString());
                    }
                }
                Thread.Sleep(SLEEP);
            }
            catch (IOException e) { } // just for unit tests, where there is no active Console
        }


        internal void CalculateNextStepsInCells()
        {
            for (int x = 0; x < Cells.GetLength(0); x++)
                for (int y = 0; y < Cells.GetLength(1); y++)
                    Cells[x, y].CalculateNextStep();
        }


        internal void SwitchCellsToNextStep()
        {
            for (int x = 0; x < Cells.GetLength(0); x++)
                for (int y = 0; y < Cells.GetLength(1); y++)
                    Cells[x, y].SwitchToNextStep();
        }



        public Cell GetCellAt(Coordinate coordinate)
        {
            if (coordinate > TopRightCorner)
                throw new GameOfLifeException($"Invalid coordinate {coordinate}");
            return Cells[coordinate.Tuple.X, coordinate.Tuple.Y];
        }


        public int CountLivingCells()
        {
            int count = 0;
            for (int x = 0; x < Cells.GetLength(0); x++)
                for (int y = 0; y < Cells.GetLength(1); y++)
                    if (Cells[x, y].IsAliveNow) count++;
            return count;
        }
    }
}
