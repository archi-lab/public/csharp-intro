﻿/// Das ist eine prozedurale Implementierung aus rosettacode.org 
/// (prozedural deshalb, weil es zwar eine Klasse gibt, in der aber sämtlicher
/// Code steht - die Aufgabe wurde also eher in Methoden als in Klassen zerlegt).
/// 
/// (c) https://rosettacode.org/wiki/Conway%27s_Game_of_Life
/// Creative Commons Attribution-ShareAlike 4.0 International (CC BY-SA 4.0)

using System.Text;

namespace Intro2CSharp.GameOfLife.Procedural
{
    // Plays Conway's Game of Life on the console with a random initial state.
    class RosettaCodeGameOfLife
    {
        // The delay in milliseconds between board updates.
        private const int DELAY = 50;

        // The cell colors.
        private const ConsoleColor DEAD_COLOR = ConsoleColor.White;
        private const ConsoleColor LIVE_COLOR = ConsoleColor.Black;

        // The color of the cells that are off of the board.
        private const ConsoleColor EXTRA_COLOR = ConsoleColor.Gray;

        private const char EMPTY_BLOCK_CHAR = ' ';
        private const char FULL_BLOCK_CHAR = '\u2588';

        // Holds the current state of the board.
        private static bool[,] board;

        // The dimensions of the board in cells.
        private static int width = 32;
        private static int height = 32;

        // True if cell rules can loop around edges.
        private static bool loopEdges = true;


        static void Main(string[] args)
        {
            // Use initializeRandomBoard for a larger, random board.
            initializeDemoBoard();

            initializeConsole();

            // Run the game until the Escape key is pressed.
            while (!Console.KeyAvailable || Console.ReadKey(true).Key != ConsoleKey.Escape)
            {
                RosettaCodeGameOfLife.drawBoard();
                RosettaCodeGameOfLife.updateBoard();

                // Wait for a bit between updates.
                Thread.Sleep(DELAY);
            }
        }

        // Sets up the Console.
        private static void initializeConsole()
        {
            Console.BackgroundColor = EXTRA_COLOR;
            Console.Clear();

            Console.CursorVisible = false;

            // Each cell is two characters wide.
            // Using an extra row on the bottom to prevent scrolling when drawing the board.
            int width = Math.Max(RosettaCodeGameOfLife.width, 8) * 2 + 1;
            int height = Math.Max(RosettaCodeGameOfLife.height, 8) + 1;
            Console.SetWindowSize(width, height);
            Console.SetBufferSize(width, height);

            Console.BackgroundColor = DEAD_COLOR;
            Console.ForegroundColor = LIVE_COLOR;
        }

        // Creates the initial board with a random state.
        private static void initializeRandomBoard()
        {
            var random = new Random();

            RosettaCodeGameOfLife.board = new bool[RosettaCodeGameOfLife.width, RosettaCodeGameOfLife.height];
            for (var y = 0; y < RosettaCodeGameOfLife.height; y++)
            {
                for (var x = 0; x < RosettaCodeGameOfLife.width; x++)
                {
                    // Equal probability of being true or false.
                    RosettaCodeGameOfLife.board[x, y] = random.Next(2) == 0;
                }
            }
        }

        // Creates a 3x3 board with a blinker.
        private static void initializeDemoBoard()
        {
            RosettaCodeGameOfLife.width = 3;
            RosettaCodeGameOfLife.height = 3;

            RosettaCodeGameOfLife.loopEdges = false;

            RosettaCodeGameOfLife.board = new bool[3, 3];
            RosettaCodeGameOfLife.board[1, 0] = true;
            RosettaCodeGameOfLife.board[1, 1] = true;
            RosettaCodeGameOfLife.board[1, 2] = true;
        }

        // Draws the board to the console.
        private static void drawBoard()
        {
            // One Console.Write call is much faster than writing each cell individually.
            var builder = new StringBuilder();

            for (var y = 0; y < RosettaCodeGameOfLife.height; y++)
            {
                for (var x = 0; x < RosettaCodeGameOfLife.width; x++)
                {
                    char c = RosettaCodeGameOfLife.board[x, y] ? FULL_BLOCK_CHAR : EMPTY_BLOCK_CHAR;

                    // Each cell is two characters wide.
                    builder.Append(c);
                    builder.Append(c);
                }
                builder.Append('\n');
            }

            // Write the string to the console.
            Console.SetCursorPosition(0, 0);
            Console.Write(builder.ToString());
        }

        // Moves the board to the next state based on Conway's rules.
        private static void updateBoard()
        {
            // A temp variable to hold the next state while it's being calculated.
            bool[,] newBoard = new bool[RosettaCodeGameOfLife.width, RosettaCodeGameOfLife.height];

            for (var y = 0; y < RosettaCodeGameOfLife.height; y++)
            {
                for (var x = 0; x < RosettaCodeGameOfLife.width; x++)
                {
                    var n = countLiveNeighbors(x, y);
                    var c = RosettaCodeGameOfLife.board[x, y];

                    // A live cell dies unless it has exactly 2 or 3 live neighbors.
                    // A dead cell remains dead unless it has exactly 3 live neighbors.
                    newBoard[x, y] = c && (n == 2 || n == 3) || !c && n == 3;
                }
            }

            // Set the board to its new state.
            RosettaCodeGameOfLife.board = newBoard;
        }

        // Returns the number of live neighbors around the cell at position (x,y).
        private static int countLiveNeighbors(int x, int y)
        {
            // The number of live neighbors.
            int value = 0;

            // This nested loop enumerates the 9 cells in the specified cells neighborhood.
            for (var j = -1; j <= 1; j++)
            {
                // If loopEdges is set to false and y+j is off the board, continue.
                if (!RosettaCodeGameOfLife.loopEdges && y + j < 0 || y + j >= RosettaCodeGameOfLife.height)
                {
                    continue;
                }

                // Loop around the edges if y+j is off the board.
                int k = (y + j + RosettaCodeGameOfLife.height) % RosettaCodeGameOfLife.height;

                for (var i = -1; i <= 1; i++)
                {
                    // If loopEdges is set to false and x+i is off the board, continue.
                    if (!RosettaCodeGameOfLife.loopEdges && x + i < 0 || x + i >= RosettaCodeGameOfLife.width)
                    {
                        continue;
                    }

                    // Loop around the edges if x+i is off the board.
                    int h = (x + i + RosettaCodeGameOfLife.width) % RosettaCodeGameOfLife.width;

                    // Count the neighbor cell at (h,k) if it is alive.
                    value += RosettaCodeGameOfLife.board[h, k] ? 1 : 0;
                }
            }

            // Subtract 1 if (x,y) is alive since we counted it as a neighbor.
            return value - (RosettaCodeGameOfLife.board[x, y] ? 1 : 0);
        }
    }
}