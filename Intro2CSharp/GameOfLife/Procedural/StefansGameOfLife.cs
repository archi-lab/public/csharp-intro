﻿/// (c) Stefan Bente 2022

namespace Intro2CSharp.GameOfLife.Procedural
{
    internal class StefansGameOfLife
    {
        public static void InitAndRun()
        {
            // the data structures needed for visualizing the cell state
            (int x, int y) topRightCorner = (115, 25);
            const char ALIVE_CELL = '\u2588';
            const char DEAD_CELL = ' ';
            char[,] cells = new char[topRightCorner.x + 1, topRightCorner.y + 1];
            char[,] futureCells = new char[topRightCorner.x + 1, topRightCorner.y + 1];
            (int, int)[] startPattern = { (40, 10), (41, 10), (42, 10), (42, 11), (41, 12) };
            double initialLivingProbability = 0.1;
            const int SLEEP = 20;

            /*
            // initialize cells with random values
            Random random = new Random();
            for (int x = 0; x < cells.GetLength(0); x++)
            {
                for (int y = 0; y < cells.GetLength(1); y++)
                {
                    bool isAlive = (random.NextDouble() < initialLivingProbability);
                    cells[x, y] = isAlive ? ALIVE_CELL : DEAD_CELL;
                }
            }
            */

            // alternatively: initialize cells with a given start pattern
            for (int x = 0; x < cells.GetLength(0); x++)
            {
                for (int y = 0; y < cells.GetLength(1); y++)
                {
                    (int, int) currentCoordinate = (x, y);
                    bool isAlive = startPattern.Contains(currentCoordinate);
                    cells[x, y] = isAlive ? ALIVE_CELL : DEAD_CELL;
                }
            }
            

            while (true)
            {
                // print all cells
                Console.SetCursorPosition(0, Console.WindowTop);
                for (int y = 0; y < cells.GetLength(1); y++)
                {
                    for (int x = 0; x < cells.GetLength(0); x++)
                    {
                        Console.SetCursorPosition(x, cells.GetLength(1) - y);
                        Console.Write(cells[x, y]);
                    }
                }
                Thread.Sleep(SLEEP);

                // calculate next state for each cell
                for (int x = 0; x < cells.GetLength(0); x++)
                {
                    for (int y = 0; y < cells.GetLength(1); y++)
                    {
                        HashSet<(int x, int y)> neighbourCoordinates = new();
                        if (x > 0) 
                            neighbourCoordinates.Add((x-1, y));
                        if (x > 0 && y > 0)
                            neighbourCoordinates.Add((x-1, y-1));
                        if (x > 0 && y < topRightCorner.y)
                            neighbourCoordinates.Add((x-1, y+1));
                        if (y > 0)
                            neighbourCoordinates.Add((x, y-1));
                        if (y < topRightCorner.y)
                            neighbourCoordinates.Add((x, y+1));
                        if (x < topRightCorner.x)
                            neighbourCoordinates.Add((x+1, y));
                        if (x < topRightCorner.x && y > 0)
                            neighbourCoordinates.Add((x+1, y-1));
                        if (x < topRightCorner.x && y < topRightCorner.y)
                            neighbourCoordinates.Add((x+1, y+1));

                        // now count how many neighbours are alive
                        int countAliveNeighbours = 0;
                        foreach (var coordinate in neighbourCoordinates)
                        {
                            if (cells[coordinate.x, coordinate.y] == ALIVE_CELL) countAliveNeighbours++;
                        }
                        bool isAliveInNextStep = (countAliveNeighbours >= 2 && countAliveNeighbours <= 3);
                        futureCells[x, y] = isAliveInNextStep ? ALIVE_CELL : DEAD_CELL;
                    }
                }
                
                // switch all to the next state
                for (int y = 0; y < cells.GetLength(1); y++)
                    for (int x = 0; x < cells.GetLength(0); x++)
                        cells[x,y] = futureCells[x,y];
            }
        }
    }
}
 