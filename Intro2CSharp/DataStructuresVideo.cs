﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace Intro2CSharp
{
    public class DataStructureExamples
    {

        public void ArrayEssentials()
        {
            string[] cars = { "Volvo", "BMW", "Ford", "Mazda" };
            int[] numbers = { 10, 20, 30, 40 };
            int[] moreNumbers = new int[5];
            int[,,] threeDimNumbers = new int[23, 777, 22];

            Console.WriteLine(cars[2]);
            Console.WriteLine(threeDimNumbers[34, -1, 21]);
            // index out of bounds in dim 0 & 1 !!

            Console.WriteLine(numbers);
        }






        public void OneDimArrayExample()
        {
            Console.WriteLine("Neues Element mit 'i <elem>', Ende mit <RET>");
            string[]? tokens;
            string[] wordArray = { "---", "---", "---", "---", "---", "---" };
            while (true)
            {
                tokens = Console.ReadLine()?.Split();
                if (tokens[0] == "") break;
                wordArray[Convert.ToInt16(tokens[0])] = tokens[1];
                Console.WriteLine("\t" + string.Join("\t", wordArray));
            }
        }



        public void TwoDimStringArrayExample()
        {
            Console.WriteLine("Neues Element mit 'i j <elem>', Ende mit <RET>");
            string[]? tokens;
            string[,] twoDimArray = {
                { "---", "---", "---", "---", "---", "---" },
                { "---", "---", "---", "---", "---", "---" },
                { "---", "---", "---", "---", "---", "---" },
                { "---", "---", "---", "---", "---", "---" },
                { "---", "---", "---", "---", "---", "---" },
                { "---", "---", "---", "---", "---", "---" }
            };
            while (true)
            {
                tokens = Console.ReadLine()?.Split();
                if (tokens[0] == "") break;
                twoDimArray[Convert.ToInt16(tokens[0]), Convert.ToInt16(tokens[1])] = tokens[2];
                for (int i = 0; i < twoDimArray.GetLength(0); i++)
                {
                    for (int j = 0; j < twoDimArray.GetLength(1); j++)
                    {
                        Console.Write($"\t{twoDimArray[i, j]}");
                    }
                    Console.WriteLine();
                }
            }
        }





        public void ListEssentials()
        {
            List<string> words = new();
            words.Add("Hallo");
            words.Add("Welt");
            Console.WriteLine(string.Join(" ", words));
            words.Insert(1, "spannende");
            Console.WriteLine(string.Join(" ", words));
        }




        public void ListExample()
        {
            Console.WriteLine("Hinzufügen mit '+ <wort>', Löschen mit '- <wort>', Ende mit <RET>");
            string[]? tokens;
            List<string> allStrings = new();
            while (true)
            {
                tokens = Console.ReadLine()?.Split();
                if (tokens?[0] == "") break;
                if (tokens?[0] == "+")
                {
                    allStrings.Add(tokens[1]);
                }
                else if (tokens?[0] == "-")
                {
                    allStrings.Remove(tokens[1]);
                }
                foreach (string element in allStrings) Console.WriteLine($"\t{element}");
            }
        }


        public void SetEssentials()
        {
            HashSet<string> set1 = new HashSet<string>();
            HashSet<string> set2 = new HashSet<string>();
            set1.Add("Hallo");
            set1.Add("Hallo");
            set1.Add("Welt");
            set2.Add("Goodbye");
            set2.Add("Welt");
            set2.IntersectWith(set1);
            Console.WriteLine(string.Join(" ", set2));
        }


        public void SetExample()
        {
            Console.WriteLine("Hinzufügen mit '+ <wort>', wegnehmen mit '- <wort>', Ende mit <RET>");
            string[]? tokens;
            HashSet<string> allStrings = new HashSet<string>();
            while (true)
            {
                tokens = Console.ReadLine()?.Split();
                if (tokens?[0] == "") break;
                if (tokens?[0] == "+")
                {
                    allStrings.Add(tokens[1]);
                }
                else if (tokens?[0] == "-")
                {
                    allStrings.Remove(tokens[1]);
                }
                foreach (string element in allStrings) Console.WriteLine($"\t\t{element}");
            }
        }



        public void HashMapEssentials()
        {
            Dictionary<string, string> hashMap = new();
            hashMap["Hallo"] = "Welt";
            hashMap["Goodbye"] = "AllMyWorries";
            hashMap.Remove("Hallo");
        }

        public void HashMapExampleCollectKeyValuePairs()
        {
            Console.WriteLine("Hinzufügen/Überschreiben mit '<key> <value>', Löschen mit '- <key>', Ende mit <RET>");
            string[]? tokens;
            Dictionary<string, string> hashMap = new();
            while (true)
            {
                tokens = Console.ReadLine()?.Split();
                if (tokens?[0] == "") break;
                if (tokens?[0] == "-")
                {
                    hashMap.Remove(tokens[1]);
                }
                else
                {
                    hashMap[tokens[0]] = tokens[1];
                }
                foreach (var item in hashMap) Console.WriteLine($"\t{item.Key}: {item.Value}");
            };
        }


        public void HashMapExampleCountStrings()
        {
            Console.WriteLine("Hinzufügen mit '<wort>', Ende mit <RET>");
            string[]? tokens;
            Dictionary<string, int> hashMap = new();
            while (true)
            {
                tokens = Console.ReadLine()?.Split();
                if (tokens?[0] == "") break;
                if (hashMap.ContainsKey(tokens[0]))
                {
                    hashMap[tokens[0]] = hashMap[tokens[0]] + 1;
                }
                else
                {
                    hashMap[tokens[0]] = 1;
                }
                foreach (var item in hashMap) Console.WriteLine($"\t{item.Key}: {item.Value}");
            };
        }




        public void StackEssentials()
        {
            Stack<string> wordStack = new();
            wordStack.Push("Hallo");
            wordStack.Push("Welt");
            Console.WriteLine($"Zuletzt hinzugefügt: {wordStack.Peek()}");
            Console.WriteLine($"Oberstes Element entfernt: {wordStack.Pop()}");
        }



        public void StackExample()
        {
            Console.WriteLine("Hinzufügen mit '<wort>', Herausgeben mit '-', Ende mit <RET>");
            string token;
            Stack<string> stack = new();
            while (true)
            {
                token = Console.ReadLine();
                if (token == "") break;
                if (token == "-")
                {
                    Console.WriteLine($"\t{stack.Pop()}");
                }
                else
                {
                    stack.Push(token);
                }
            }
        }



        public void QueueEssentials()
        {
            Queue<string> wordQueue = new();
            wordQueue.Enqueue("Hallo");
            wordQueue.Enqueue("Welt");
            Console.WriteLine($"Zuletzt hinzugefügt: {wordQueue.Peek()}");
            Console.WriteLine($"Oberstes Element entfernt: {wordQueue.Dequeue()}");
        }



        public void QueueExample()
        {
            Console.WriteLine("Hinzufügen mit '<wort>', Herausgeben mit '-', Ende mit <RET>");
            string token;
            Queue<string> queue = new();
            while (true)
            {
                token = Console.ReadLine();
                if (token == "") break;
                if (token == "-")
                {
                    Console.WriteLine($"\t{queue.Dequeue()}");
                }
                else
                {
                    queue.Enqueue(token);
                }
            }
        }
    }
}
